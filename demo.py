# -*- coding: utf-8 -*-
#
# MLHub demonstrator and toolkit for pixellib.
#
# Time-stamp: <Monday 2022-03-07 08:35:04 +1100 Graham Williams>
#
# Authors: Graham.Williams@togaware.com
# License: General Public License v3 GPLv3
# License: https://www.gnu.org/licenses/gpl-3.0.en.html
# Copyright: (c) Graham Williams. All rights reserved.

from pixellib.torchbackend.instance import instanceSegmentation

ins = instanceSegmentation()
ins.load_model("pointrend_resnet50.pkl")
ins.segmentImage("image.jpg",
                 show_bboxes=True,
                 output_image_name="output_image.jpg")
